using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Entities
{
    public class Operations<T>
    {
        protected string Data;
        protected List<T> JsonData;
        private void LoadJson()
        {
            if(typeof(T) == typeof(PersonDetails))
            {
                Data = File.ReadAllText(@"Repository\PersonData.JSON");
            }

            else if(typeof(T) == typeof(BookDetails))
            {
                Data = File.ReadAllText(@"Repository\BookData.JSON");
            }

            else if(typeof(T) == typeof(CarDetails))
            {
                Data = File.ReadAllText(@"Repository\CarData.JSON");
            }
            JsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(Data);
        }

        private void WriteJson(string newJson)
        {
            if(typeof(T) == typeof(PersonDetails))
            {
                File.WriteAllText(@"Repository\PersonData.JSON",newJson);
            }

            else if(typeof(T) == typeof(BookDetails))
            {
                File.WriteAllText(@"Repository\BookData.JSON",newJson);;
            }

            else if(typeof(T) == typeof(CarDetails))
            {
                File.WriteAllText(@"Repository\CarData.JSON",newJson);
            }
        }

        public void Read() 
        {
            LoadJson();
            var properties = typeof(T).GetProperties();
            foreach (var item in JsonData)
            {
                foreach (var property in properties)
                {
                    Console.WriteLine($"{property.Name} = {property.GetValue(item,null)}");
                }
            }
            
        }
    
        public void Insert()
        {
            LoadJson();
            T newEntity = (T)Activator.CreateInstance(typeof(T));
            var properties = newEntity.GetType().GetProperties();
            foreach (var item in properties)
            {
                Console.WriteLine("Enter the {0}",item.Name);
                
                if(item.PropertyType.Name != "String")
                {
                   item.SetValue(newEntity,Convert.ChangeType(Console.ReadLine(),item.PropertyType));    
                }
                else
                {
                    item.SetValue(newEntity,Console.ReadLine());
                }               
            }
            JsonData.Add(newEntity);
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(JsonData);
            WriteJson(newJson);    
        }
        public void Delete()
        {
            LoadJson();
            object newEntity = Activator.CreateInstance(typeof(T));
            var properties = newEntity.GetType().GetProperties();
            foreach (var item in properties)
            {
                Console.WriteLine("Enter the {0}",item.Name);
                
                if(item.PropertyType.Name != "String")
                {
                    //item.SetValue(newEntity,Convert.ToInt32(Console.ReadLine()));
                    item.SetValue(newEntity,Convert.ChangeType(Console.ReadLine(),item.PropertyType));    
                }
                else
                {
                    item.SetValue(newEntity,Console.ReadLine());
                }               
            }
            int Flag = 0;
            int Flag2 ;
            foreach(var data in JsonData)
            {
                Flag2 = 1;
                foreach (var property in properties)
                {   
                    if(property.GetValue(data,null).Equals( property.GetValue(newEntity,null)))
                    {
                        Flag2 = 1;
                    }
                    else
                    {
                        Flag2 = 0;
                        break;
                    }
                }
                if(Flag2 == 1)
                {
                    JsonData.Remove(data);
                    Flag = 1;
                    break;
                }
            }
            if(Flag == 0)
            {
                Console.WriteLine("No Such Record Exists");
            }
            else
            {
                Console.WriteLine("Record Deleted sucessfully");
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(JsonData);
            WriteJson(newJson);    
            
        }
        public void Update()
        {
            LoadJson();
            Console.WriteLine("Enter the details of the entity you want to update");
            T newEntity = (T)Activator.CreateInstance(typeof(T));
            var properties = newEntity.GetType().GetProperties();
            foreach (var item in properties)
            {
                Console.WriteLine("Enter the {0}",item.Name);
                
                if(item.PropertyType.Name != "String")
                {
                    item.SetValue(newEntity,Convert.ChangeType(Console.ReadLine(),item.PropertyType));   
                }
                else
                {
                    item.SetValue(newEntity,Console.ReadLine());
                }               
            }
            int Flag = 0;
            int Flag2 ;
            foreach(var data in JsonData)
            {
                Flag2 = 1;
                foreach (var property in properties)
                {   
                    if(property.GetValue(data,null).Equals( property.GetValue(newEntity,null)))
                    {
                        Console.WriteLine($"Enter the new {property.Name}");
                        if(property.PropertyType.Name != "String")
                        {
                            property.SetValue(newEntity,Convert.ChangeType(Console.ReadLine(),property.PropertyType));    
                        }
                        else
                        {
                            property.SetValue(data,Console.ReadLine());
                        }
                        Flag2 = 1;
                    }
                    else
                    {
                        Flag2 = 0;
                        break;
                    }
                }
                if(Flag2 == 1)
                {
                    Flag = 1;
                    break;
                }
            }
            if(Flag == 0)
            {
                Console.WriteLine("No Such Record Exists");
            }
            else
            {
                Console.WriteLine("Record Updated sucessfully");
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(JsonData);
            WriteJson(newJson);    
               
        }
    }
}