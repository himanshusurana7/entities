using System;
using System.Collections.Generic;
using System.IO;
using Interfaces;

namespace Entities
{
    public class BookDetails
    {
        public string Name {get;set;}
        public string Author {get;set;}
        public int Price{get;set;}
    }
}
    /*class Book:ICRUDInterface
    {   
        public List<BookDetails> book{get;set;}
        protected string strData;
        protected Book bookData;

        protected void LoadFiles()
        {
            strData = File.ReadAllText(@"Repository\BookData.Json");
            bookData = Newtonsoft.Json.JsonConvert.DeserializeObject<Book>(strData);
        } 

        public void Delete()
        {
            this.LoadFiles(); 
            BookDetails newBook = new BookDetails();
            Console.WriteLine("Enter the Name of book");
            newBook.Name = Console.ReadLine();
            Console.WriteLine("Enter the Author's Name");
            newBook.Author = Console.ReadLine();
            Console.WriteLine("Enter the Price");
            newBook.Price = Convert.ToInt32(Console.ReadLine());
            int Flag = 0;
            foreach(var data in bookData.book)
            {
                if(data.Name == newBook.Name && data.Author == newBook.Author && data.Price == newBook.Price)
                {
                    bookData.book.Remove(data);
                    Flag = 1;
                    break;
                }
            }
            if(Flag == 0)
            {
                Console.WriteLine("No Such Book Exists");
            }
            else
            {
                Console.WriteLine("Book Deleted sucessfully");
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(bookData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\BookData.Json",newJson);
        }

        public void Insert()
        {
            this.LoadFiles();
            BookDetails newBook = new BookDetails();
            Console.WriteLine("Enter the Name of the book");
            newBook.Name = Console.ReadLine();
            Console.WriteLine("Enter the Author's Name");
            newBook.Author = Console.ReadLine();
            Console.WriteLine("Enter the Price");
            newBook.Price = Convert.ToInt32(Console.ReadLine());
            
            bookData.book.Add(newBook);
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(bookData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\BookData.Json",newJson);
            
        }

        public void Read()
        {
            this.LoadFiles();
            foreach(var data in bookData.book)
            {
                Console.WriteLine("{0}      {1}      {2}",data.Name,data.Author,data.Price);
            }
        }

        public void Update()
        {
            this.LoadFiles();
            Console.WriteLine("Enter the details of the book you want to update");
            BookDetails newBook = new BookDetails();
            Console.WriteLine("Enter the Name of the book");
            newBook.Name = Console.ReadLine();
            Console.WriteLine("Enter the Author's Name");
            newBook.Author = Console.ReadLine();
            Console.WriteLine("Enter the Price");
            newBook.Price = Convert.ToInt32(Console.ReadLine());

            foreach(var data in bookData.book)
            {
                if(data.Name == newBook.Name && data.Author == newBook.Author && data.Price == newBook.Price)
                {
                    Console.WriteLine("Enter the new name");
                    data.Name = Console.ReadLine();
                    Console.WriteLine("Enter the new Author");
                    data.Author = Console.ReadLine();
                    Console.WriteLine("Enter the new Price");
                    data.Price = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Record Updated sucessfully.");
                    break;
                }
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(bookData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\BookData.Json",newJson);
               
        }
    }
}*/