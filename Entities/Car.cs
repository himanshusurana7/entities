using System;
using System.Collections.Generic;
using System.IO;
using Interfaces;

namespace Entities
{
    public class CarDetails
    {
        public string Model {get;set;}
        public string RcNo {get;set;}
        public int Age{get;set;}
    }
}
    /*class Car:ICRUDInterface
    {
        public List<CarDetails> car{get;set;}
        protected string strData;
        protected Car carData; 

        protected void LoadFiles()
        {
            strData = File.ReadAllText(@"Repository\CarData.JSON");
            carData = Newtonsoft.Json.JsonConvert.DeserializeObject<Car>(strData);
        }
        public void Delete()
        {
            this.LoadFiles();
            CarDetails newCar = new CarDetails();
            Console.WriteLine("Enter the Model of car");
            newCar.Model = Console.ReadLine();
            Console.WriteLine("Enter the RcNo");
            newCar.RcNo = Console.ReadLine();
            Console.WriteLine("Enter the Age");
            newCar.Age = Convert.ToInt32(Console.ReadLine());
            int Flag = 0;
            foreach(var data in carData.car)
            {
                if(data.Model == newCar.Model && data.RcNo == newCar.RcNo && data.Age == newCar.Age)
                {
                    carData.car.Remove(data);
                    Flag = 1;
                    break;
                }
            }
            if(Flag == 0)
            {
                Console.WriteLine("No Such Car Exists");
            }
            else
            {
                Console.WriteLine("Car Deleted sucessfully");
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(carData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\CarData.JSON",newJson);
        }

        public void Insert()
        {
            this.LoadFiles();
            CarDetails newCar = new CarDetails();
            Console.WriteLine("Enter the Model of the car");
            newCar.Model = Console.ReadLine();
            Console.WriteLine("Enter the RcNo");
            newCar.RcNo = Console.ReadLine();
            Console.WriteLine("Enter the Age");
            newCar.Age = Convert.ToInt32(Console.ReadLine());
            
            carData.car.Add(newCar);
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(carData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\CarData.JSON",newJson);
            
        }

        public void Read()
        {
            this.LoadFiles();
            foreach(var data in carData.car)
            {
                Console.WriteLine("{0}      {1}      {2}",data.Model,data.RcNo,data.Age);
            }
        }

        public void Update()
        {
            this.LoadFiles();
            Console.WriteLine("Enter the details of the car you want to update");
            CarDetails newCar = new CarDetails();
            Console.WriteLine("Enter the Model of the car");
            newCar.Model = Console.ReadLine();
            Console.WriteLine("Enter the RcNo of the car");
            newCar.RcNo = Console.ReadLine();
            Console.WriteLine("Enter the Age of the car");
            newCar.Age = Convert.ToInt32(Console.ReadLine());

            foreach(var data in carData.car)
            {
                if(data.Model == newCar.Model && data.RcNo == newCar.RcNo && data.Age == newCar.Age)
                {
                    Console.WriteLine("Enter the new Model");
                    data.Model = Console.ReadLine();
                    Console.WriteLine("Enter the new RcNo");
                    data.RcNo = Console.ReadLine();
                    Console.WriteLine("Enter the new Age");
                    data.Age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Record Updated sucessfully.");
                    break;
                }
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(carData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\CarData.JSON",newJson);
               
        }
    }
}*/