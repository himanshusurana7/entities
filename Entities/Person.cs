using System;
using Interfaces;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;   
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Entities
{
    public class PersonDetails
    {
        public string FName {get;set;}
        public string LName {get;set;}
        public int Age{get;set;}

    }
}    

    /*public class Person: ICRUDInterface
    {
        public List<PersonDetails> person{get;set;}
         
        protected string strData;
        protected Person personData;

        protected void LoadFiles()
        {
            strData = File.ReadAllText(@"Repository\PersonData.Json");
            personData = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>(strData);
        }
        public void Delete()
        {
            //var strData = File.ReadAllText(@"C:\Users\himanshu.surana\source\repos\SampleApplication\Repository\PersonData.JSON");
            //var personData = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>(strData);
            this.LoadFiles();
            PersonDetails newPerson = new PersonDetails();
            Console.WriteLine("Enter the First Name");
            newPerson.FName = Console.ReadLine();
            Console.WriteLine("Enter the Last Name");
            newPerson.LName = Console.ReadLine();
            Console.WriteLine("Enter the Age");
            newPerson.Age = Convert.ToInt32(Console.ReadLine());
            int Flag = 0;
            foreach(var data in personData.person)
            {
                if(data.FName == newPerson.FName && data.LName == newPerson.LName && data.Age == newPerson.Age)
                {
                    personData.person.Remove(data);
                    Flag = 1;
                    break;
                }
            }
            if(Flag == 0)
            {
                Console.WriteLine("No Such Person Exists");
            }
            else
            {
                Console.WriteLine("Person Deleted sucessfully");
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(personData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\PersonData.JSON",newJson);
        }

        public void Insert()
        {
            //var strData = File.ReadAllText(@"C:\Users\himanshu.surana\source\repos\SampleApplication\Repository\PersonData.JSON");
            //var personData = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>(strData);
            this.LoadFiles();
            PersonDetails newPerson = new PersonDetails();
            Console.WriteLine("Enter the First Name");
            newPerson.FName = Console.ReadLine();
            Console.WriteLine("Enter the Last Name");
            newPerson.LName = Console.ReadLine();
            Console.WriteLine("Enter the Age");
            newPerson.Age = Convert.ToInt32(Console.ReadLine());
            
            personData.person.Add(newPerson);
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(personData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\PersonData.JSON",newJson);
            
        }

        public void Read()
        {
           // var strData = File.ReadAllText(@"C:\Users\himanshu.surana\source\repos\SampleApplication\Repository\PersonData.JSON");
            //var personData = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>(strData);
            this.LoadFiles();
            //Console.WriteLine("First Name   Last Name   Age");
            foreach(var data in personData.person)
            {
                Console.WriteLine("{0}      {1}      {2}",data.FName,data.LName,data.Age);
            }
        }

        public void Update()
        {
            //var strData = File.ReadAllText(@"C:\Users\himanshu.surana\source\repos\SampleApplication\Repository\PersonData.JSON");
            //var personData = Newtonsoft.Json.JsonConvert.DeserializeObject<Person>(strData);
            this.LoadFiles();
            Console.WriteLine("Enter the details of the person you want to update");
            PersonDetails newPerson = new PersonDetails();
            Console.WriteLine("Enter the First Name");
            newPerson.FName = Console.ReadLine();
            Console.WriteLine("Enter the Last Name");
            newPerson.LName = Console.ReadLine();
            Console.WriteLine("Enter the Age");
            newPerson.Age = Convert.ToInt32(Console.ReadLine());

            foreach(var data in personData.person)
            {
                if(data.FName == newPerson.FName && data.LName == newPerson.LName && data.Age == newPerson.Age)
                {
                    Console.WriteLine("Enter the new first name");
                    data.FName = Console.ReadLine();
                    Console.WriteLine("Enter the new last name");
                    data.LName = Console.ReadLine();
                    Console.WriteLine("Enter the new Age");
                    data.Age = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Record Updated sucessfully.");
                    break;
                }
            }
            var newJson = Newtonsoft.Json.JsonConvert.SerializeObject(personData);
            //Console.WriteLine(newJson);
            File.WriteAllText(@"Repository\PersonData.JSON",newJson);
               
        }
    }
}*/