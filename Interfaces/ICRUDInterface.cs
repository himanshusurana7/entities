using System;
namespace Interfaces
{
    public interface ICRUDInterface
    {
        public void Insert();
        public void Read();
        public void Update();
        public void Delete();
    }
}