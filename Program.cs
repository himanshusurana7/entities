﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Entities;

namespace SampleApplication
{
    class Program
    {
        public static void Menu2(string EntityType)
        {
            object obj;
            if(EntityType == "Person")
            {
                obj = new Operations<PersonDetails>();
            }
            else if(EntityType == "Book")
            {
                obj = new Operations<BookDetails>();   
            }
            else            
            {
                obj = new Operations<CarDetails>(); 
            }
            var methods = obj.GetType().GetMethods(BindingFlags.DeclaredOnly|BindingFlags.Instance|BindingFlags.Public);
            /*foreach (var item in methods)
            {
                Console.WriteLine(item.Name);
            }*/
            int option2 = 9;
            Console.WriteLine($"Select your operation\n1.Press 1 to View the existing {EntityType} \n2.Press 2 to Add a new {EntityType}\n3.Press 3 to remove an existing {EntityType}\n4.Press 4 to update an exisiting {EntityType}\n0.Press 0 to return to main menu");
            option2= Convert.ToInt32(Console.ReadLine());
            if(option2 == 0)
            {
               return;
            }
            else if(option2 == 1)
            {
                methods[0].Invoke(obj,null);    
            }
            else if(option2 == 2)
            {
                methods[1].Invoke(obj,null);
            }
            else if(option2 == 3)
            {
                methods[2].Invoke(obj,null);
            }
            else if(option2 == 4)
            {
                methods[3].Invoke(obj,null);
            }
        }

        static void Main(string[] args)
        {
            int option = 9;
            while(option != 0)
            {
                //main menu
                Console.WriteLine("Select your Entity type\n1.Press 1 for Person Type\n2.Press 2 for Book Type\n3.Press 3 for Car Type\n0. Press 0 to exit");
                try
                {
                    option = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception e)
                {
                    Console.WriteLine($"{e.Message} : Enterd option is not valid Re-Enter you option");
                    continue;
                }
                if(option == 1)
                {
                    Menu2("Person");
                }
                else if(option == 2)
                {
                    Menu2("Book");
                }
                else if(option == 3)
                {
                    Menu2("Car");
                }
            }
            Console.WriteLine("Thank You for using Entities");
        }
    }
}
